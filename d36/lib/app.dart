import 'package:flutter/material.dart';

import 'package:d36/ui/screens/login.dart';
import 'package:d36/ui/screens/home.dart';
import 'package:d36/ui/theme.dart';

class D36App extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'District 36',
      theme:buildTheme(),
      routes: {
        '/': (context) => HomeScreen(),
        '/login': (context) => LoginScreen(),
      },
    );
  }
}