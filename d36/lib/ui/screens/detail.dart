import 'package:flutter/material.dart';

import 'package:d36/state_widget.dart';
import 'package:d36/model/story.dart';
import 'package:d36/model/state.dart';
import 'package:d36/utils/store.dart';
import 'package:d36/ui/widgets/story_title.dart';
import 'package:d36/ui/widgets/story_image.dart';

class DetailScreen extends StatefulWidget {
  final Story story;
  final bool liked;

  DetailScreen(this.story, this.liked);

  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class StoryView extends StatelessWidget {
  final Story story;

  StoryView(this.story);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Text(story.content),
      ),
    );
  }
}

class _DetailScreenState extends State<DetailScreen> with SingleTickerProviderStateMixin {
  TabController _tabController;
  ScrollController _scrollController;
  bool _liked;
  StateModel appState;

  @override
  initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: 2);
    _scrollController = ScrollController();
    _liked = widget.liked;
  }
  @override
  void dispose() {
    _tabController.dispose();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    appState = StateWidget.of(context).state;

    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (BuildContext context, bool innerViewIsScrolled) {
          return <Widget>[
            SliverAppBar(
              backgroundColor: Colors.white,
              flexibleSpace: FlexibleSpaceBar(
                collapseMode: CollapseMode.pin,
                background: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    StoryImage(widget.story.imageURL),
                    StoryTitle(widget.story, 25.0),
                  ],
                ),
              ),
            expandedHeight: 300.0,
            pinned: true,
            floating: true,
            elevation: 2.0,
            forceElevated: innerViewIsScrolled,
            )
          ];
        },
      body: Column(
        children: <Widget>[
          StoryView(widget.story), 
        ]
      ),
      ),
    );
  }
}