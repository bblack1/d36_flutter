import 'package:flutter/material.dart';
import 'package:d36/state_widget.dart';
import 'package:flutter/painting.dart';
import 'dart:math';

import 'package:d36/ui/widgets/google_sign_in_button.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    BoxDecoration _buildBackground() {
      int backgroundId = Random().nextInt(3);
      return BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/backgrounds/$backgroundId.JPG"),
          fit: BoxFit.cover,
        )
      );
    }
    Image _buildLogo() {
      return Image.asset('assets/logos/D36Comp.png');
    }

  return Scaffold(
    body: Container(
      decoration: _buildBackground(),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildLogo(),
            SizedBox(height: 50.0,),
            GoogleSignInButton(
              onPressed: () => StateWidget.of(context).signInWithGoogle(),
            ),
          ],
        ),
      ),
    )
  );
  }
}