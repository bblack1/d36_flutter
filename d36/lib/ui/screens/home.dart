import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:d36/model/state.dart';
import 'package:d36/model/story.dart';
import 'package:d36/model/calendar_event.dart';

import 'package:d36/ui/screens/login.dart';

import 'package:d36/ui/widgets/settings_button.dart';
import 'package:d36/ui/widgets/story_card.dart';
import 'package:d36/ui/widgets/calendar_tile.dart';

import 'package:d36/utils/store.dart';

import 'package:d36/state_widget.dart';


class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  // New member of the class:
  StateModel appState;

  // New method:
  // Inactive widgets are going to call this method to
  // signalize the parent widget HomeScreen to refresh the list view.
  void _handleFavoritesListChanged(String storyID) {
    updateLiked(appState.user.uid, storyID).then((result) {
      if (result == true) {
        setState(() {
          if (!appState.liked.contains(storyID)) appState.liked.add(storyID);
          else appState.liked.remove(storyID);
        });
      }
    });
  }
  
    Center _buildLoadingIndicator() {
      return Center(
        child: new CircularProgressIndicator(),
      );
    }

    DefaultTabController _buildTabView({Widget body}) {
      const double _iconSize = 20.0;

      return DefaultTabController(
        length: 4,
        child: Scaffold(
          appBar: PreferredSize(
            // We set Size equal to passed height (50.0) and infinite width:
            preferredSize: Size.fromHeight(50.0),
            child: AppBar(
              backgroundColor: Colors.white,
              elevation: 2.0,
              bottom: TabBar(
                labelColor: Theme.of(context).indicatorColor,
                tabs: [
                  Tab(icon: Icon(Icons.new_releases, size: _iconSize)),
                  Tab(icon: Icon(Icons.calendar_today, size: _iconSize)),
                  Tab(icon: Icon(Icons.list, size: _iconSize)),
                  Tab(icon: Icon(Icons.settings, size: _iconSize)),
                ],
              ),
            ),
          ),
          body: Padding(
            padding: EdgeInsets.all(5.0),
            child: body,
          ),
        ),
      );
    }

    TabBarView _buildTabsContent() {
      Padding _buildStories() {
        CollectionReference collectionReference = Firestore.instance.collection('stories');
        Stream<QuerySnapshot> stream;

        stream = collectionReference.snapshots();

        return Padding(
          padding: const EdgeInsets.symmetric(vertical:  5.0),
          child: Column(
            children: <Widget>[
              Expanded(
                child: new StreamBuilder(
                  stream: stream,
                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData) return _buildLoadingIndicator();
                    return new ListView(
                      children: snapshot.data.documents.map((document) {
                        return new StoryCard(
                          story: Story.fromMap(document.data, document.documentID),
                          liked: appState.liked.contains(document.documentID),
                          onLikedButtonPressed: _handleFavoritesListChanged,
                        );
                      }).toList(),
                    );
                  }
                ),
              )
            ],
          )
        );
      }

      return TabBarView(
        children: [
          _buildStories(),
          _buildCalendar(),
          _buildStories(),
          _buildSettings(),
        ],
      );
    }

    Column _buildCalendar() {
      CollectionReference collectionReference = Firestore.instance.collection('calander_events');
        Stream<QuerySnapshot> stream;

        stream = collectionReference.snapshots();
      return Column (
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: new StreamBuilder(
                  stream: stream,
                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (!snapshot.hasData) return _buildLoadingIndicator();
                    return new ListView(
                      children: snapshot.data.documents.map((document) {
                        return new CalendarTile(
                          event: Event.fromMap(document.data, document.documentID)
                        );
                      }).toList(),
                    );
                  }
                ),
          )
        ],
      );
    }

    Column _buildSettings() {
      return Column (
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SettingsButton(
            Icons.exit_to_app,
            "Log Out",
            appState.user.displayName, 
            () async {
              StateWidget.of(context).signOutOfGoogle();
            }
          )
        ],
      );
    }

    Widget _buildContent(){
      if (appState.isLoading) {
        return _buildTabView(
          body: _buildLoadingIndicator()
        );
      } else if (!appState.isLoading && appState.user == null) {
        return new LoginScreen();
      } else {
        return _buildTabView(
          body: _buildTabsContent()
        );
      }
    }
    @override
    Widget build(BuildContext context) {
      appState = StateWidget.of(context).state;
      return _buildContent();
    }
  }