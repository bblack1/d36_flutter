import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:d36/state_widget.dart';
import 'package:d36/ui/widgets/story_image.dart';
import 'package:d36/model/calendar_event.dart';
import 'package:d36/model/state.dart';
import 'package:d36/utils/store.dart';

class EventScreen extends StatefulWidget {
  final Event event;

  EventScreen(this.event);

  @override
  _EventScreenState createState() => _EventScreenState();
}

class EventView extends StatelessWidget {
  final Event event;

  EventView(this.event);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(event.name),
    );
  }
}

class _EventScreenState extends State<EventScreen> with SingleTickerProviderStateMixin {
  ScrollController _scrollController;
  StateModel appState;

  @override
  initState() {
    super.initState();
    _scrollController = ScrollController();
  }
  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  Text _title(event) {
    String _titleString = "Round ${event.round}: ${event.name}";
    return Text(
      _titleString,
      style: Theme.of(context).textTheme.title
    );
  }

  _launchURL(url) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  Column _buildTitleSection(event) {
    String month = event.date.toDate().month.toString();
    String day = event.date.toDate().day.toString();
    String year = event.date.toDate().year.toString();
    String dateText = "$month/$day/$year";
    return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    StoryImage(widget.event.eventImageUrl),
                    Padding(
                      padding: EdgeInsets.all(5),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          _title(event),
                          Text(dateText),
                          SizedBox(height: 10.0,),
                          Row(
                            children: [
                              Text("Hosted by: ${widget.event.promotor}"),
                              SizedBox(width: 5.0),
                            ],
                          )
                        ],
                      )
                    ),
                  ],
                );
  }

  Column _buildBody(event) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    _launchURL(event.flyerUrl);
                    },
                  child: Icon(Icons.description, size: 50),
                ),
                Text("Flyer"),
              ],
            ),
            Column(
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    
                    String mapUrl = "http://maps.google.com/maps?q=${event.location.latitude},${event.location.longitude}";
                    _launchURL(mapUrl);
                  },
                  child: Icon(Icons.map, size: 50),
                ),
                Text("Directions"),
              ],
            ),
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    appState = StateWidget.of(context).state;

    return Scaffold(
      body: NestedScrollView(
        controller: _scrollController,
        headerSliverBuilder: (BuildContext context, bool innerViewIsScrolled) {
          return <Widget>[
            SliverAppBar(
              backgroundColor: Colors.white,
              flexibleSpace: FlexibleSpaceBar(
                collapseMode: CollapseMode.pin,
                background: _buildTitleSection(widget.event),
              ),
            expandedHeight: 300.0,
            pinned: true,
            floating: true,
            elevation: 2.0,
            forceElevated: innerViewIsScrolled,
            )
          ];
        },
        body: _buildBody(widget.event)
      ),
    );
  }
}