import 'package:flutter/material.dart';

import 'package:d36/model/story.dart';

class StoryTitle extends StatelessWidget {
  final Story story;
  final double padding;

  StoryTitle(this.story, this.padding);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(padding),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            story.title,
            style: Theme.of(context).textTheme.title,
          ),
          SizedBox(height: 10.0,),
          Row(
            children: [
              SizedBox(width: 5.0),
            ],
          )
        ],
      )
    );
  }
}