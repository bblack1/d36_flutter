import 'package:d36/ui/screens/detail.dart';
import 'package:flutter/material.dart';

import 'package:d36/model/story.dart';
import 'package:d36/ui/widgets/story_title.dart';
import 'package:d36/ui/widgets/story_image.dart';

class StoryCard extends StatelessWidget {
  final Story story;
  final bool liked;
  final Function onLikedButtonPressed;

  StoryCard(
    {
      @required this.story,
      @required this.liked,
      @required this.onLikedButtonPressed,
    }
  );

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new DetailScreen(story, liked)
        )
      ),
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        child: Card(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  AspectRatio(
                    aspectRatio: 16.0 / 9.0,
                    child: Image.network(
                      story.imageURL,
                      fit: BoxFit.cover,
                    ),
                  ),
                ],
              ),
              StoryTitle(story, 15),
            ],
          )
        )
      )
    );
  }
}