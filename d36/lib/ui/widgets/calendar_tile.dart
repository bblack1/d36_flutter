import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:d36/ui/screens/event.dart';

import 'package:d36/model/calendar_event.dart';

class CalendarTile extends StatelessWidget {
  final Event event;

  CalendarTile(
    {
      @required this.event,
    }
  );

  Image clubIcon(clubImage) {
    return new Image(
      image: NetworkImage(clubImage),
      color: null,
      fit: BoxFit.scaleDown,
      alignment: Alignment.center,
    );
  }

  Text buildDateText(event) {
    String month = event.date.toDate().month.toString();
    String day = event.date.toDate().day.toString();
    String dateString = "${month}/${day}";
    return Text(dateString);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => new EventScreen(event)
        )
      ),
      child: Card(
        child: ListTile(
          leading: clubIcon(event.logoUrl),
          title: Text(event.name),
          subtitle: buildDateText(event),
        ),
      )
    );
  }
}

