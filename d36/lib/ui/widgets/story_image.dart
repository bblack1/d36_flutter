import 'package:flutter/material.dart';
import 'package:flutter/semantics.dart';

class StoryImage extends StatelessWidget {
  final String imageURL;

  StoryImage(this.imageURL);

  @override
  Widget build(BuildContext contest) {
    return AspectRatio(
      aspectRatio: 16.0 / 9.0,
      child: Image.network(
        imageURL,
        fit: BoxFit.cover,
      )
    );
  }
}