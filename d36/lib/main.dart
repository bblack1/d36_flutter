import 'package:flutter/material.dart';
import 'package:d36/app.dart';
import 'package:d36/state_widget.dart';


 
void main() => runApp(
  new StateWidget(
    child: new D36App(),
  )
);