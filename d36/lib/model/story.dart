import 'package:flutter/cupertino.dart';

class Story {
  final String id;
  final String title;
  final String summary;
  final String content;
  final String imageURL;

  const Story({
    this.id,
    this.title,
    this.summary,
    this.content,
    this.imageURL,
  });

  Story.fromMap(Map<String, dynamic> data, String id): this(
    id: id,
    title: data['title'],
    summary: data['summary'],
    content: data['content'],
    imageURL: data['imageURL'],
  );
}