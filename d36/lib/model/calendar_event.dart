import 'package:cloud_firestore/cloud_firestore.dart';

class Event {
  final String id;
  final String name;
  final Timestamp date;
  final GeoPoint location;
  final bool national;
  final bool points;
  final String promotor;
  final bool signupEnabled;
  final String signupLink;
  final String logoUrl;
  final String eventImageUrl;
  final int round;
  final String flyerUrl;

  const Event({
    this.id,
    this.name,
    this.date,
    this.location,
    this.national,
    this.points,
    this.promotor,
    this.signupEnabled,
    this.signupLink,
    this.logoUrl,
    this.eventImageUrl,
    this.round,
    this.flyerUrl,
  });

  Event.fromMap(Map<String, dynamic> data, String id): this(
    id: id,
    name: data['name'],
    date: data['date'],
    location: data['location'],
    national: data['national'],
    points: data['points'],
    promotor: data['promotor'],
    signupEnabled: data['signup_enabled'],
    signupLink: data['signup_link'],
    logoUrl: data['club_logo_url'],
    eventImageUrl: data['event_image_url'],
    round: data['round'],
    flyerUrl: data['flyer_url'],
  );
}