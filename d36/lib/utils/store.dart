import 'package:cloud_firestore/cloud_firestore.dart';

Future<bool> updateLiked(String uid, String storyId) {
  DocumentReference likedReference = Firestore.instance.collection('users').document(uid);

  return Firestore.instance.runTransaction((Transaction tx) async {
    DocumentSnapshot postSnapshot = await tx.get(likedReference);
    if (postSnapshot.exists) {
      if (!postSnapshot.data['liked'].contains(storyId)) {
        await tx.update(likedReference, <String, dynamic>{'liked': FieldValue.arrayUnion([storyId])
        });
      } else {
        await tx.update(likedReference, <String, dynamic>{'liked': FieldValue.arrayRemove([storyId])
        });
      }
    } else {
      await tx.set(likedReference, {
        'liked': [storyId]
        }
      );
    }
  }).then((result) {
    return true;
  }).catchError((error) {
    print('Error: $error');
    return false;
  });
}